<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use app\models\Product;
use yii\widgets\DetailView;

$this->title = 'Product - Single Product View';
$this->params['breadcrumbs'][] = $this->title;


$model = Product::findOne($id);

?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',              
        ],
    ]);
    
    
    ?> 


    <code><?= __FILE__ ?></code>
</div>
